# Makefile for zmac

# Edit any defs which are wrong - these should be OK for Linux though.

CC	?= gcc

YACC	:= bison -y

CFLAGS	?= -O -Wall

PREFIX	?= /usr/local
BINDIR	?= $(PREFIX)/bin
MANDIR	?= $(PREFIX)/share/man/man1


all:	zmac

zmac:	zmac.c mio.c getoptn.c
	$(CC) $(CFLAGS) -o zmac zmac.c mio.c getoptn.c

zmac.c: zmac.y
	$(YACC) zmac.y -o zmac.c

install: zmac
	install -s -m 511 zmac $(BINDIR)
	install -m 444 zmac.1 $(MANDIR)

clean:
	$(RM) *.o zmac.c *~ zmac
